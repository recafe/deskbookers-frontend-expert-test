/**
 * Desk list component
 *
 * Created by Koralis on 16.3.4.
 */
(function (window, google)
{
    var desk_list_component = (function ()
    {
        function desk_list_component(id, opts)
        {
            this.id = id;
            this.element = $("#" + id);
            this.defaultSettings =
            {

            };
            this.settings = $.extend(true, {}, this.defaultSettings, opts);

            this.prepareTemplate();
        }

        desk_list_component.prototype = {
            /**
             * Add a desk
             *
             * @param desk
             */
            addDesk: function (desk)
            {
                $.tmpl("template", desk).appendTo(this.element);
            },

            /**
             * Remove all the desks
             */
            clearDesks: function ()
            {
                this.element.html('');
            },

            /**
             * Using jQuery template plugin for this demo
             */
            prepareTemplate: function ()
            {
                var template =
                    "<div class='desk col-sm-6'>" +
                        "<div>" +
                            "<div class='image-holder'>" +
                                "<img src='${image}' class='img-responsive'/>" +
                                "<div class='rating'>${location_rating} / 10</div>" +
                            "</div>" +
                            "<div class='info'>" +
                                "<div class='info-right'>" +
                                    "<div class='hour-price'>" +
                                        "<span>${hour_price}&euro;</span>/uur" +
                                    "</div>" +
                                "</div>" +
                                "<h2>${name}</h2>" +
                                "<small>${location_name}</small>" +
                                "<small>${location_city}</small>" +
                            "</div>" +
                        "</div>" +
                    "</div>";
                $.template("template", template);
            },
        };

        return desk_list_component;
    }());

    desk_list_component.create = function (element, opts)
    {
        return new desk_list_component(element, opts);
    };

    window.desk_list_component = desk_list_component;

}(window, google));