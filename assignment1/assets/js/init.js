$(document).ready(function () {

    /**
     * Load the map component
     *
     * @type {gMap}
     */
    map = map_component.create('gMap');

    /**
     * Load the desk list component
     * @type {desks}
     */
    desk_list_component = desk_list_component.create('desks');

    /**
     * Load the search component
     * @type {map}
     */
    search = search_component.create(map, desk_list_component);
})