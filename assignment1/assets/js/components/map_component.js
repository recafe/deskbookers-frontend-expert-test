/**
 * Map component
 *
 * Created by Koralis on 16.3.4.
 */
(function (window, google)
{
    var map_component = (function ()
    {
        function map_component(id, opts)
        {
            this.id = id;
            this.markers = [];
            this.defaultSettings =
            {
                map:
                {
                    zoom: 10,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center:
                    {
                        lat: 52.293753,
                        lng: 4.634942
                    },
                    MarkerClusterer: true
                }
            };
            this.settings = $.extend(true, {}, this.defaultSettings, opts);
            this.prepare(id);
        }

        map_component.prototype = {
            /**
             * Initialize the map
             */
            prepare: function ()
            {
                this.map = new google.maps.Map(document.getElementById(this.id), this.settings.map);
            },

            /**
             * Create the MarkerClusterer instance with the current markers
             */
            markerClusterer: function ()
            {
                if(this.settings.map.MarkerClusterer)
                {
                    if (this.MarkerClusterer)
                    {
                        this.MarkerClusterer.clearMarkers();
                    }
                    this.MarkerClusterer = new MarkerClusterer(this.map, this.markers);
                }
            },

            /**
             * Add a marker
             *
             * @param marker_data
             */
            addMarker: function (marker_data)
            {
                self = this;
                $.extend(true, marker_data, {map: self.map});
                var marker = new google.maps.Marker(marker_data);
                self.markers.push(marker);
                self.markerClusterer();
            },

            /**
             * Remove all markers
             */
            removeMarkers: function ()
            {
                self = this;
                for (var i = 0; i < self.markers.length; i++)
                {
                    self.markers[i].setMap(null);
                }

                self.markers = [];
                self.markerClusterer();
            }
        };

        return map_component;
    }());

    map_component.create = function (element, opts)
    {
        return new map_component(element, opts);
    };

    window.map_component = map_component;

}(window, google));