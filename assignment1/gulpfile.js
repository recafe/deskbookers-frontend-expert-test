var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('default', function () {
    gulp.src('assets/scss/app.scss').pipe(sass()).pipe(gulp.dest('assets/build/css'));
});

gulp.task('watch', function() {
    gulp.watch('assets/scss/*', ['default']);
});