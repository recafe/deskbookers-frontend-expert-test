/**
 * Desk list component
 *
 * Created by Koralis on 16.3.4.
 */
(function (window, google)
{
    var search_component = (function ()
    {
        function search_component(map, desk_list_component, opts)
        {
            this.map = map;
            this.desks = desk_list_component;
            this.defaultSettings = {
                url: "https://www.deskbookers.com/nl-nl/sajax.json",
                data: {
                    q: "Amsterdam",
                    type: "-",
                    people: "any",
                    favorite: 0,
                    pid: "",
                    sort: "relevance",
                    sw: "",
                    ne: "",
                    ids: ""
                },
                yql_url: 'https://query.yahooapis.com/v1/public/yql'
            };
            this.settings = $.extend(true, {}, this.defaultSettings, opts);
            this.getData();
            this.addMapListener();
        }

        search_component.prototype = {
            /**
             * Query and get the data
             */
            getData: function ()
            {
                var self = this;
                this.buildUrl();

                $.ajax({
                    url: "https://jsonp.afeld.me/?url=" + encodeURIComponent(self.settings.finalUrl),
                    'success': function (response)
                    {
                        self.build(response);
                    }
                });
            },

            /**
             * Prepare the url for getting the data
             */
            buildUrl: function ()
            {
                var url = this.settings.url + "?" + $.param(this.settings.data);
                this.settings.finalUrl = url;
            },

            /**
             * Build the main information table
             * @param data
             */
            build: function (data)
            {
                var places = [];
                var self = this;
                self.map.removeMarkers();
                self.desks.clearDesks();
                $(data).each(function (index, object)
                {
                    var desk = {
                        "name": object.name,
                        "location_name": object.location_name,
                        "location_city": object.location_city,
                        "image": object.image_urls[0],
                        "location_rating": object.location_rating.toFixed(0),
                        "hour_price": object.hour_price
                    };

                    self.map.addMarker({
                        position: {lat: object.coordinate[0], lng: object.coordinate[1]},
                        title: object.name,
                        customInfo: object.id
                    });
                    self.desks.addDesk(desk);
                });
            },

            /**
             * Listen for changes on zoom (etc.) and update the map
             */
            addMapListener: function ()
            {
                var self = this;
                google.maps.event.addListener(this.map.map, 'idle', function (event)
                {
                    var bounds = self.map.map.getBounds();
                    self.settings.data.sw = (bounds.R.R).toFixed(6) + "," + (bounds.j.R).toFixed(6);
                    self.settings.data.ne = (bounds.R.j).toFixed(6) + "," + (bounds.j.j).toFixed(6);
                    self.getData();
                });
            },
        };

        return search_component;
    }());

    search_component.create = function (map, desk_list_component, opts)
    {
        return new search_component(map, desk_list_component, opts);
    };

    window.search_component = search_component;

}(window, google));